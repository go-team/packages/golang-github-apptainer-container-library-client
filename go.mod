module github.com/apptainer/container-library-client

go 1.17

require (
	github.com/blang/semver/v4 v4.0.0
	github.com/go-log/log v0.2.0
	github.com/stretchr/testify v1.7.0
	github.com/sylabs/json-resp v0.8.0
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
